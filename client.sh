##
# bash client for coinharvest rigs
#
# Dependencies:
# - curl
# - jq
#
# Usage:
# source this script wherever client is needed
# call the functions with respect to their documentation
##

WEBGUI_ROOT="https://dev2.coinharvest.io"

##
# Authenticate rig, claim it if not already claimed, and get a token for future requests
# Run this after booting up. There is no need to call it again until next boot.
#
# Required variables:
# USERNAME - coinharvest username
# PASSWORD - coinharvest password
# RIGNAME - name of the rig
# RIGTOKEN - token generated for the rig
#
# Sets COINHARVEST_AUTH_* and INFLUX_* vars for future usage
#
# Optional variables:
# USE_WEBGUI - YES or NO
# USE_METRICS - YES or NO
#
##
WEBGUI_AUTH(){
if [ $USE_WEBGUI == "YES" ]; then
    RIGTOKEN=$(ifconfig -a | grep -Eo ..\(\:..\){5} | openssl enc -base64)
    COINHARVEST_AUTH_PAYLOAD="{\"username\":\"$USERNAME\", \"rigname\":\"$RIGNAME\", \"rigtoken\":\"$RIGTOKEN\"}"
    COINHARVEST_AUTH_RESPONSE=$(curl -s -f -X POST -H 'Content-Type: application/json' -d "$COINHARVEST_AUTH_PAYLOAD" $WEBGUI_ROOT/api/v1/token)
    if [[ $? -eq "0" ]]; then
      COINHARVEST_AUTH_TOKEN=$(echo $COINHARVEST_AUTH_RESPONSE | jq -r '.token')
      COINHARVEST_AUTH_HEADER="Authorization: Bearer $COINHARVEST_AUTH_TOKEN"
      if [ $USE_METRICS == "YES" ]; then
        INFLUX_CREDS=$(curl -s -H "$COINHARVEST_AUTH_HEADER" $WEBGUI_ROOT/api/v1/influx)
        INFLUX_ENDPOINT=$(echo $INFLUX_CREDS | jq -r '.endpoint')
        INFLUX_DATABASE=$(echo $INFLUX_CREDS | jq -r '.database')
        INFLUX_USERNAME=$(echo $INFLUX_CREDS | jq -r '.username')
        INFLUX_PASSWORD=$(echo $INFLUX_CREDS | jq -r '.password')
      fi
    else
      echo "ERROR: Failed to authenticate to webgui. Did you add the rig in the webgui? $COINHARVEST_AUTH_RESPONSE"
      return 1
    fi
fi
}

##
# Send metrics to influxdb
# Requires a successful WEBGUI_AUTH and USE_WEBGUI=yes and USE_METRICS=yes
# Requires that your CoinHarvest user is has the donator role
#
# Examples:
#
# Write one measurement:
#
#   INFLUX_WRITE "current_hashrate,host=$RIGNAME value=$CURRENTHASH"
#
# Write multiple measurements at once:
#
#   INFLUX_WRITE "gpu_temp,host=$RIGNAME,gpu_device=$i value=${TEMPERATURE[i]}
#                 gpu_powerdraw,host=$RIGNAME,gpu_device=$i value=${POWER[i]}"
##
INFLUX_WRITE() {
if [ $USE_METRICS == "YES" ]; then
  curl -XPOST "$INFLUX_ENDPOINT/write?u=$INFLUX_USERNAME&p=$INFLUX_PASSWORD&db=$INFLUX_DATABASE" --data-binary "$@"
fi
}

##
# Ask the server to check user's donator status via pools (all active rigs), chains, or overrides
# This is useful for ensuring that your donator role is set on the server-side
# NOTE: The development server will always make your user a donator unless FORCE_NO applied
#
# Requires a successful WEBGUI_AUTH and USE_WEBGUI=yes
#
# Optional Arguments:
# "FORCE_NO" - force the server to remove your donator status for testing purposes
#
# Prints "YES" or "NO"
#
# Examples:
#
# Update server-side donation role
#
#   VERIFY_DONATING
#
# Update server-side donation role while setting client-side variable
#
#   DONATING=$(VERIFY_DONATING)
#
# Update server-side and client-side donation states to "NO"
#
#   DONATING=$(VERIFY_DONATING "FORCE_NO")
##
VERIFY_DONATING() {
if [ $USE_WEBGUI == "YES" ]; then
  if [ "$1" == "FORCE_NO" ]; then
    DONATION_CHECK_RESPONSE=$(curl -s -H "$COINHARVEST_AUTH_HEADER" -X DELETE "$WEBGUI_ROOT/api/v1/vip/drop")
  else
    DONATION_CHECK_RESPONSE=$(curl -s -H "$COINHARVEST_AUTH_HEADER" "$WEBGUI_ROOT/api/v1/vip/verify")
  fi
  echo $DONATION_CHECK_RESPONSE | jq -r '.donating'
fi
}
