USERNAME="admin@coinharvest.io"
RIGNAME="cube"
RIGTOKEN="can-be-anything-but-must-stay-consistent-for-that-rigname-username-combo"

USE_WEBGUI="YES"
USE_METRICS="YES"

source $(dirname $0)/client.sh

WEBGUI_AUTH

INFLUX_WRITE "gpu_temp,host=$RIGNAME,gpu_device=0 value=40
              gpu_powerdraw,host=$RIGNAME,gpu_device=0 value=100"

echo "DONATING: $(VERIFY_DONATING)"
echo "DONATING: $(VERIFY_DONATING "FORCE_NO")"

echo "THIS SHOULD FAIL"

INFLUX_WRITE "gpu_temp,host=$RIGNAME,gpu_device=0 value=40
              gpu_powerdraw,host=$RIGNAME,gpu_device=0 value=100"


echo "DONATING: $(VERIFY_DONATING)"

echo "THIS SHOULD NOT FAIL"

INFLUX_WRITE "gpu_temp,host=$RIGNAME,gpu_device=0 value=40
              gpu_powerdraw,host=$RIGNAME,gpu_device=0 value=100"

echo "DONE"
